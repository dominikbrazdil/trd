# test_results_reporting deployment

## AWS Key
Save file with aws key locally and use it for connecting



## Deploying trd

if on new instance, save AWS credentials for S3 to /etc/environment

```
bash autoDeploy.sh -p "ip adress of ec2 instance -i "path to file with key" -n (parameter -n for deploying on new instance)
```

script creates package, ssh to instance, installs dependencies and runs service   

!! AWS credentials (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY) need to be set in environment variables

to open port on dev-test environment
```
firewall-cmd --permanent --add-port=8080/tcp
firewall-cmd --reload
```