#!/bin/bash

new=$1
version=$2

# on new instance install dependencies
if [[ $new = 'true' ]] ; then
    # make possible installing packages (change https_proxy based on used vpn)
    # sudo sh -c 'echo -e "no_proxy=localhost,127.0.0.1,169.254.169.254,.concurasp.com,.nonprod.cnqr.tech,.s3-us-west-2.amazonaws.com,.concurtech.org,.concurtech.net,.concurawsdev.com,.cnqr.io,.cnqr.delivery\nhttps_proxy=http://internal-us-dev-proxy-classic-477080641.us-west-2.elb.amazonaws.com:3128" >> /etc/environment'
    sudo sh -c 'echo -e "no_proxy=localhost,127.0.0.1,169.254.169.254,.concurasp.com,.nonprod.cnqr.tech,.s3-us-west-2.amazonaws.com,.concurtech.org,.concurtech.net,.concurawsdev.com,.cnqr.io,.cnqr.delivery\nhttps_proxy=http://proxy-us-aws.nonprod.cnqr.tech:3128" >> /etc/environment'

    sudo yum install -y python36 python36-pip
    sudo pip3.6 install --upgrade pip

    sudo mkdir /opt/testReporting/
    # add rights to ec2-user
    sudo chown ec2-user: /opt/testReporting/

fi

# remove old version
sudo pip3.6 uninstall test-results-reporting -y
# install new version
sudo pip3.6 install test_results_reporting-$version-py3-none-any.whl

# copy files to right directories
sudo cp -r /usr/local/lib/python3.6/site-packages/etc/. /etc/
sudo cp /usr/local/lib/python3.6/site-packages/system/test-results-reporting-server.service /usr/lib/systemd/system/


# run service
echo 'running service'
sudo systemctl daemon-reload
sudo systemctl restart test-results-reporting-server
sleep 1
sudo systemctl status test-results-reporting-server
