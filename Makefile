VERSION=$(shell cat version.txt)

all:
	@echo "make pip | test | clean"

test:
	pytest \
		-v \
		--junit-xml junit.xml \
		--cov-config .coveragerc \
		--cov testReporting \
		--cov test-results-reporting.py \
		--cov-report term \
		--cov-report xml \
		tests/test_*.py

clean:
	rm -rf build __pycache__ htmlcov coverage.xml \
		.coverage .cache .pytest_cache junit.xml
	find . -type f -name '*.pyc' -exec rm {} \;
	find . -type d -name __pycache__ -prune -exec rm -r {} \;

pip:
	cp -r pkg build
	> build/README
	cp conf/config.json build/test-results-reporting-conf.json
	cp -r testReporting build/
	cp teams.json build/teams.json
	cp test-results-reporting.py build/test-results-reporting-server
	sed "s/__VERSION__/$(VERSION)/" setup.py > build/setup.py
	cd build && \
		python3 setup.py bdist_wheel
