from flask_wtf import FlaskForm
from wtforms import SelectField, StringField


class DashForm(FlaskForm):
    timestamps = [(0, 'Since the beginning of time'),
                  (30*24, 'Last 30 days'),
                  (7*24, 'Last 7 days'),
                  (24, 'Last 24 hours'),
                  (1, 'Last hour')]
    timestampSelectField = SelectField("Time range", choices=timestamps)

    searchTextField = StringField("Submit")
