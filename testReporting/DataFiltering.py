
from datetime import datetime, timedelta


class DataFiltering:
    """Sorting test after querying, filtering tests"""

    def filterTests(self, tests, params):
        """
        Filter tests in testPage by all params
        :param tests: dictionary of tests
        :param params: dictionary of params you wnt to sort by
        :return: dictionary of filtered tests
        """
        filteredTests = tests
        for param, value in params.items():
            if param == 'timestamp':
                filteredTests = self.filterTime(filteredTests, value)
                continue
            if value == 'All':
                continue
            result = []
            for test in tests:
                if test[param] == value:
                    result.append(test)
            filteredTests = result
        return filteredTests

    def filterTime(self, tests, since):
        now = datetime.now() - timedelta(hours=since)

        if since == 0:
            return tests
        result = []
        for test in tests:
            date = datetime.strptime(test['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
            if date > now:
                result.append(test)
            else:
                break  # tests should be sorted by timestamp
        return result

    def filterTestsSearchBar(self, tests, inputSearch):
        if inputSearch is "":
            return tests

        # result = (test for test in tests if 'name' in test and inputSearch.lower() in test['name'].lower())
        result = []
        for test in tests:
            if 'name' in test and inputSearch.lower() in test['name'].lower():
                result.append(test)

        return result

    def filterSearchBar(self, tests, inputSearch):
        """
        Filtering using search bar
        :param inputSearch: Filtering expression
        """
        inputSearch = inputSearch.replace(" ", "")

        if inputSearch is "":
            return tests

        if ":" not in inputSearch:
            desiredKey = inputSearch
            desiredSubkey = ""
        else:
            desiredKey, desiredSubkey = inputSearch.split(":")

        result = tests.copy()

        if desiredKey is not "":
            result = {k: v for k, v in result.items() if desiredKey in k}
        if desiredSubkey is not "":
            for key, value in result.items():
                result[key] = {k: v for k, v in result[key].items() if desiredSubkey in k}
            tmp = result.copy()
            for key, value in tmp.items():
                if len(tmp[key]) == 0:
                    result.pop(key)

        return result

    def filterStatsTime(self, stats, since):
        now = datetime.now() - timedelta(hours=since)

        if since == 0:
            return stats
        result = {}
        for key, value in stats.items():
            for subkey, stat in value.items():
                date = datetime.strptime(stat['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
                if date > now:
                    if key not in result.keys():
                        result[key] = {}
                    result[key][subkey] = stat
        return result

    def countStats(self, tests):
        """
        Counting test statistics for testPage status bar
        :return: dictionary with test statistics
        """
        stats = {
            'passed': 0,
            'skipped': 0,
            'failed': 0
        }
        for test in tests:
            stats[test['status']] += 1
        return stats

    def removeDuplicates(self, tests):
        """
        Filter out every duplicate run of the same test and leave the latest
        """
        result = []
        temp = {}
        for test in tests:
            if not test['path'] in temp.keys():
                temp[test['path']] = test['timestamp']
                result.append(test)  # works if tests are sorted by timestampuú
        return result

