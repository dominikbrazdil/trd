import json
import logging
import os
import sys

from flask import Flask, render_template, redirect, url_for, request, jsonify

from testReporting.DataDriver import DataDriver
from testReporting.DataFiltering import DataFiltering
from testReporting.forms.testResultsForm import TestResultsForm
from testReporting.forms.dashForm import DashForm

app = Flask(__name__)
app.config['SECRET_KEY'] = '12345'


# Initialization before running flask app
def initApp(conf, local):
    global driver, parser, config, logger

    with open(conf, 'r') as f:
        config = json.load(f)
    driver = DataDriver(conf, local)
    parser = DataFiltering()
    logger = logging.getLogger('flask-web')


@app.route('/', methods=['GET', 'POST'])
def root():
    with open(config["TEAMS_FILE"], 'r') as f:
        teams = json.load(f)

    if request.method == 'POST':
        for key in request.form.keys():
            value = request.form[key]
            break

        team, platform = value.split("_")

        return redirect(url_for('dash', team=team, platform=platform))

    return render_template('root.html', teams=teams)


@app.route('/<team>/<platform>/<testVersion>', methods=['GET', 'POST'])
def testResults(team, platform, testVersion):
    form = TestResultsForm()

    platformName = "" if platform == "None" else platform
    tests = driver.getSpecificData(team, platformName, testVersion)

    if form.is_submitted():
        # ticket creating
        if 'createTicket' in request.form:
            driver.saveTicketToS3(team,
                                  platformName,
                                  request.form['testPath'],
                                  request.form['ticketName'],
                                  request.form['ticketType'])

        params = {
            'status': form.statusSelectField.data,
            'timestamp': int(form.timestampSelectField.data),
        }
        searchbarInput = form.searchTextField.data
        tests = parser.filterTestsSearchBar(
            parser.filterTests(tests, params), searchbarInput)

        if form.duplicatesCheckbox.data:
            tests = parser.removeDuplicates(tests)

    stats = parser.countStats(tests)

    # passed arguments are accessible in template through jinja2
    return render_template(getTestPageTemplate(team),
                           result=tests,
                           form=form,
                           stats=stats,
                           team=team,
                           platform=platform)


@app.route('/<team>/<platform>', methods=['GET', 'POST'])
def dash(team, platform):
    form = DashForm()

    platformName = "" if platform == "None" else platform
    stats = driver.parseStatsFromFile(team, platformName)

    if form.is_submitted():
        timestamp = int(form.timestampSelectField.data)
        searchbarInput = form.searchTextField.data
        stats = parser.filterSearchBar(
            parser.filterStatsTime(stats, timestamp), searchbarInput)

    return render_template(getDashTemplate(team),
                           result=stats,
                           form=form,
                           team=team,
                           platform=platform)


@app.route('/jira', methods=['GET', 'POST'])
def loadTickets():
    team = request.args.get('team')
    platform = request.args.get('platform')
    path = request.args.get('path')
    tickets = driver.getTicketsByTest(team, platform, path)
    return jsonify(tickets)


def startFlask(host, port, conf, local):
    initApp(conf, local)
    app.run(debug=True, host=host, port=port)


def getDashTemplate(team):
    name = team + '.html'
    if templateExists(name):
        return name
    else:
        return 'dashTemplate.html'


def getTestPageTemplate(team):
    name = team + 'Tests.html'
    if templateExists(name):
        return name
    else:
        return 'testPageTemplate.html'


def templateExists(name):
    templates = os.listdir(os.path.dirname(sys.modules[__name__].__file__) + '/templates/')
    for template in templates:
        if name == template:
            return True
    return False
