function hideFunction(x) {
      var x = document.getElementById(x);

      if (x.style.display === "none") {
         x.style.display = "contents";
      } else {
        x.style.display = "none";
      }

}

// passing test id to modal form
$('#ticketModalForm').on('show.bs.modal', function(e) {

    //get data attribute of the clicked element
    var path = $(e.relatedTarget).data('path');

    //populate the hidden textbox
    $(e.currentTarget).find('input[name="testPath"]').val(path);
});

// ajax request for getting jira tickets at run-time for each test
$(document).ready(function(){
        $(".tickets").each(function() {
            var div = $(this);
            $.ajax({
                url: '/jira',
                type: 'get',
                data: {
                    team: div.attr("data-team"),
                    platform: div.attr("data-platform"),
                    path: div.attr("data-path")
                },
                success: function(data){
                    $.each(data, function(name, type) {
                        $('<div><img title=' + type + ' class="warning" src="/static/img/' + type + '.png"><a href="https://jira.concur.com/browse/'+ name +'">' + name + '</a></div>').appendTo(div);

                    });
                }
            });
        });
});
