from __future__ import print_function  # Python 2/3 compatibility

import logging
from datetime import datetime, timedelta
import json
import os
from enum import Enum

import boto3

from testReporting.DataFiltering import DataFiltering


class Type(Enum):
    DATA = "Data.json"
    STATS = "Stats.json"


class DataDriver:
    """Handles loading from and into text files"""

    def __init__(self, conf, local):
        """
        Setup paths where to save files
        :param conf: path to config file
        """
        self.parser = DataFiltering()
        with open(conf, 'r') as f:
            self.config = json.load(f)
        self.local = local
        self.data_folder = self.config['DATA_FOLDER']
        self.jira_folder = self.config['JIRA_FOLDER']
        self.bucket = self.config['BUCKET']
        self.last_update = {}
        self.cashed_data = {}
        self.logger = logging.getLogger('data-driver')

    def getPossibleOptions(self, data, param):
        """
        Collect all possible options occurring under one parameter
        :param data: dictionary of tests
        :param param: parameter
        :return: dictionary
        """
        options = ['All']
        for key, value in data.items():
            if not value[param] in options:
                options.append(value[param])

        dictOpt = []
        for opt in options:
            dictOpt.append((opt, opt))

        return dictOpt

    def parseDataFromFile(self, team, platform):
        return self.parseFromFile(team, platform, Type.DATA)

    def getSpecificData(self, team, platform, key):
        return self.parseDataFromFile(team, platform)[key]

    def parseStatsFromFile(self, team, platform):
        return self.parseFromFile(team, platform, Type.STATS)

    def parseFromFile(self, team, platform, type):
        """
        Loads data from cash or S3 bucket to dictionary
        """
        file_name = self.getFileName(team, platform, type)
        now = datetime.now()

        if self.local and file_name not in self.last_update:
            with open('testReporting/'+self.data_folder + file_name) as f:
                self.cashed_data[file_name] = json.load(f)
            self.last_update[file_name] = now

        # if data in cache are older than 20 minutes, load them again from s3
        elif file_name not in self.last_update or self.last_update[file_name] + timedelta(minutes=20) < now:
            obj = boto3.client('s3') .get_object(Bucket=self.bucket, Key=self.data_folder + file_name)
            self.cashed_data[file_name] = json.loads(obj['Body'].read())
            self.last_update[file_name] = now
            self.logger.info("loaded file " + file_name + " from s3")

        return self.cashed_data[file_name]

    def getFileName(self, team, platform, type):
        if platform == "_":
            platform = ""
        return team + platform + type.value

    def saveTicketToS3(self, team, platform, path, name, type):
        if self.local:
            return
        boto3.resource('s3').Bucket(self.bucket).put_object(
            Key=os.path.join(self.jira_folder, team, platform, path, name + "_" + type)
        )

    def getTicketsByTest(self, team, platform, path):
        if self.local:
            return
        bucket = boto3.resource('s3').Bucket(self.bucket)
        folder = os.path.join(self.jira_folder, team, platform, path) + "/"
        tickets = {}
        for obj in bucket.objects.filter(Prefix=folder):
            ticket = obj.key.rsplit('/', 1)[1]
            name = ticket.rsplit('_')[0]
            type = ticket.rsplit('_')[1]
            tickets[name] = type
        return tickets
