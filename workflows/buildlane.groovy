import com.concur.*;

concurPipeline = new ConcurCommands()
concurUtil = new Util()

public build(Map yml, Map args) {
        docker.withRegistry('https://quay.cnqr.delivery', 'cmbuildrobot'){
            docker.image("mobileqa/trd:latest").inside{
                sh 'make clean pip'
            }
	    }
}

return this;