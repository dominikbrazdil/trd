#!/bin/bash -e
yum clean all
yum install -y epel-release
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum update -y
yum install -y python36 python36-libs python36-devel python36-pip python36-wheel
yum -y install ruby-devel gcc make rpm-build rubygems
gem install --no-ri --no-rdoc fpm

python3.6 -m venv ~/envs/py3env
source ~/envs/py3env/bin/activate
pip install --upgrade pip
pip install -r /home/vagrant/dev/test_results_reporting/requirements.txt
pip install -r /home/vagrant/dev/test_results_reporting/dev-requirements.txt
deactivate

systemctl stop firewalld
systemctl disable firewalld
