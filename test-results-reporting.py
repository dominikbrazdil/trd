#!/usr/bin/env python
import argparse
import logging
import sys

from testReporting import FlaskWeb

logging.basicConfig()
logger = logging.getLogger('test-results-reporting')


def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--listen', default='0.0.0.0')
    parser.add_argument('-c', '--conf', default='/etc/test-results-reporting-conf.json')
    parser.add_argument('-p', '--port', default=80, type=int)
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('-e', '--exported', action='store_true')
    return parser.parse_args()


def main():
    """
    Running the web app
    """
    rc = 0
    args = read_args()
    if args.debug:
        logging.root.setLevel(logging.DEBUG)
    else:
        logging.root.setLevel(logging.INFO)
    logger.info('Listening on http://%s:%s.', args.listen, args.port)
    try:
        FlaskWeb.startFlask(args.listen, args.port, args.conf, args.exported)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logger.exception('Unexpected exception.')
        logger.debug('Unexpected exception.', exc_info=True)
        rc = 1
    logger.info('Finished.')
    return rc


if __name__ == '__main__':
    sys.exit(main())
