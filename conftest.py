import json
import logging
import os

import pytest

from testReporting import FlaskWeb
from testReporting.DataDriver import DataDriver
from testReporting.DataFiltering import DataFiltering
from tests import utils


def pytest_addoption(parser):
    parser.addoption("--conf", action="store", default="conf/local.json")


def pytest_configure(config):
    global teams_file
    teams_file = config_json(config.getoption('conf'))['TEAMS_FILE']


def pytest_generate_tests(metafunc):
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixturenames".
    config_file = metafunc.config.option.conf
    if "conf" in metafunc.fixturenames and config_file is not None:
        metafunc.parametrize("conf", [config_file], scope="session")


@pytest.fixture
def some_test_data(driver, some_test_stats, some_team_and_platform):
    team = some_team_and_platform[0]
    platform = some_team_and_platform[1]
    keys = utils.get_one_test_group(some_test_stats)
    return driver.getSpecificData(team, platform, keys[0] + ":" + keys[1])


@pytest.fixture
def some_test_stats(driver, some_team_and_platform):
    team = some_team_and_platform[0]
    platform = some_team_and_platform[1]
    return driver.parseStatsFromFile(team, platform)


@pytest.fixture(scope="session")
def some_team_and_platform():
    return load_team_file()[0]


@pytest.fixture(scope="session")
def client(conf):
    FlaskWeb.initApp(conf)
    client = FlaskWeb.app.test_client()
    yield client


@pytest.fixture(scope="session")
def config_json(conf):
    with open(conf, 'r') as f:
        return json.load(f)


@pytest.fixture(scope="session")
def driver(conf):
    driver = DataDriver(conf)
    yield driver


@pytest.fixture(scope="session")
def filtering():
    filter = DataFiltering()
    yield filter


@pytest.fixture
def logger():
    return logging.getLogger('testing')


def load_team_file():
    targets = []
    with open(teams_file, 'r') as f:
        teams = json.load(f)
    for team, platforms in teams.items():
        for platform in platforms:
            targets.append((team, platform))
    return targets
