from setuptools import setup, find_packages


setup(
    name='test-results-reporting',
    version='__VERSION__',
    release='2',
    description='Test Results Reporting',
    url='https://github.concur.com/MATE/test_results_reporting',
    license='proprietary',
    scripts=['test-results-reporting-server'],
    packages=find_packages(),
    install_requires=['Flask',
                      'Flask-WTF',
                      'WTForms',
                      'boto3'],
    include_package_data=True,
    package_data={'': ['templates/*', 'static/*/*']},
    data_files=[
        ('/etc', ['test-results-reporting-conf.json']),
        ('/etc', ['teams.json']),
        ('/system',  ['test-results-reporting-server.service'])])
