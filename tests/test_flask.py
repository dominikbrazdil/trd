import json

import pytest

from conftest import load_team_file
from tests import utils


def test_home(client):
    rv = client.get('/')
    assert 200 == rv.status_code


@pytest.mark.parametrize("team, platform", load_team_file())
def test_team_endpoints(client, driver, team, platform):

    endpoint = '/'+team+'/'+platform
    print("Accessing " + endpoint)
    rv = client.get(endpoint)
    assert 200 == rv.status_code

    platformName = "" if platform == "None" else platform
    stats = driver.parseStatsFromFile(team, platformName)
    if not stats:
        return

    keys = utils.get_one_test_group(stats)

    endpoint = '/' + team + '/' + platform + '/' + keys[0] + ':' + keys[1]
    print("Accessing " + endpoint)
    rv = client.get(endpoint)
    assert 200 == rv.status_code
