from datetime import datetime, timedelta

import pytest


@pytest.mark.parametrize("filter_param, filter_value", [
    ('status', 'passed')
])
def test_filter_tests(filtering, some_test_data, filter_param, filter_value):
    results = filtering.filterTests(some_test_data, {filter_param: filter_value})
    for test in results:
        assert test[filter_param] == filter_value


@pytest.mark.parametrize("hours", [1])
def test_filter_time(filtering, some_test_data, hours):
    results = filtering.filterTime(some_test_data, hours)
    for test in results:
        date = datetime.strptime(test['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
        assert date >= datetime.now() - timedelta(hours=hours)


@pytest.mark.parametrize("filter_key, filter_subkey", [
    ('', ''),
    ('9', ''),
    ('', '0'),
    ('.6', '2')
])
def test_filter_search_bar(filtering, some_test_stats, filter_key, filter_subkey):
    results = filtering.filterSearchBar(some_test_stats, filter_key + ":" + filter_subkey)
    for key, value in results.items():
        assert filter_key in key
        for subkey, suvalue in value.items():
            assert filter_subkey in subkey