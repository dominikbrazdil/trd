

def get_one_test_group(stats):
    try:
        firstKey = next(iter(stats.keys()))
        firstSubkey = next(iter(stats[firstKey].keys()))
    except StopIteration:
        pass

    return (firstKey, firstSubkey)