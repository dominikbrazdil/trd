import os
import stat
import subprocess
import sys

import pytest


@pytest.mark.skip
def test_pep8_compliance():
    msg = "'All I'm asking for is total perfection.' -Lord Business"
    args = '--ignore=E741,E241,E722,W504'
    executable = stat.S_IEXEC | stat.S_IXGRP | stat.S_IXOTH
    for f in os.listdir('./testReporting/'):
        # skip conftest.py, report.sh, .gitignore, ..
        if os.path.isfile(f) and '.' not in f:
            f_stat = os.stat(f)
            if f_stat.st_mode & executable:
                with open(f, 'r') as fp:
                    if 'python' not in fp.readline():
                        continue
                rc = subprocess.call(
                    '{} -m pycodestyle {} {}'.format(sys.executable, args, f),
                    shell=True)
                assert not rc, msg
    rc = subprocess.call(
        '{} -m pycodestyle {} .'.format(sys.executable, args),
        shell=True)
    assert not rc, msg
