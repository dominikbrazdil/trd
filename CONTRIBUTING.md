# Developer info

## Requirements
In addition to [requirements.txt](./requirements.txt), install additional 
dev/test requirements by 
`pip install -r ` [requirements-dev.txt](.requirements-dev.txt)  

## Tests
Run tests by running `pytest`.
