#!/bin/bash

ip=''
credentials=''
new_instance='false'
version="$(cat version.txt)"

while getopts 'ni:p:' flag; do
case "${flag}" in
    n) new_instance='true' ;;
    i) credentials="${OPTARG}" ;;
    p) ip="${OPTARG}" ;;
esac
done

# create package
make clean pip
# copy package to instance
echo 'copying to instance'
scp -i $credentials build/dist/test_results_reporting-$version-py3-none-any.whl ec2-user@${ip}:~
# connect to instance
echo 'connecting to instance'
echo $credentials
ssh -i $credentials  ec2-user@${ip} 'bash -s ' $new_instance $version < serverSideDeploy.sh

